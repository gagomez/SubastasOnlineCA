package ids.subastasonlineca.Controllers;


import ids.subastasonlineca.Logistic.Auctions.LiveAuction;
import ids.subastasonlineca.Logistic.Role.Role;
import io.realm.Realm;
import io.realm.RealmResults;

public class DataBaseController {

    private Realm realm;
    private Role role;
    private Role[] roles;
    private LiveAuction[] auctionlist;

    /*Constructor , recibe la base de datos por por parametros y se la asigna al atributo "realm"*/
    public DataBaseController(Realm realm){
        this.realm = realm;
    }



    /*Retorna la base de datos*/
    public Realm getRealm() {
        return realm;
    }



    /*Crea un usuario de tipo role y lo caraga a la base de datos*/
    public void createdefaultusers() {

        this.realm.beginTransaction();
        role = realm.createObject(Role.class,"258");
        role.setEmail("g.a95");
        role.setPassword("123");
        role.setPhone("0212");
        role.setUsername("greg");
        this.realm.commitTransaction();

    }


    /*Elimina todos los elementos de la base de datos
    * Guarda los elementos en un array para luego recorrerlo y eliminar cada elemento de la base de datos*/
    public void deletedatabase(){
        RealmResults<Role> results = this.realm.where(Role.class).findAll();
        roles = results.toArray(new Role[results.size()] );

        for(int i =0 ; i<roles.length ; i++){

            this.realm.beginTransaction();
            roles[i].deleteFromRealm();
            this.realm.commitTransaction();

        }


        RealmResults<LiveAuction> results2 = this.realm.where(LiveAuction.class).findAll();
        auctionlist = results2.toArray(new LiveAuction[results2.size()] );


        for(int i =0 ; i<auctionlist.length ; i++){

            this.realm.beginTransaction();
            auctionlist[i].deleteFromRealm();
            this.realm.commitTransaction();

        }





    }




}