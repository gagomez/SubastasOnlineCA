package ids.subastasonlineca.Activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import ids.subastasonlineca.Controllers.DataBaseController;
import ids.subastasonlineca.Logistic.Auctions.LiveAuction;
import ids.subastasonlineca.Logistic.Role.Role;
import ids.subastasonlineca.R;
import io.realm.Realm;

public class CreateAccountActivity extends AppCompatActivity {

    private Intent intent;
    private Bundle bundle;
    private TextView username;
    private TextView password;
    private TextView auctioneerid;
    private TextView email;
    private TextView phone;
    private Button registerbutton;
    private DataBaseController databasecontroller;
    private Role role;
    public Realm realm;
    private LiveAuction sub1;
    private LiveAuction sub2;

    @Override
protected void onCreate(Bundle savedInstanceState) {
super.onCreate(savedInstanceState);
setContentView(R.layout.activity_createaccount);


username = (TextView) findViewById(R.id.user_register);
password = (TextView) findViewById(R.id.password_register);
auctioneerid = (TextView) findViewById(R.id.auctioneerid_register);
email = (TextView) findViewById(R.id.email_register);
phone = (TextView) findViewById(R.id.phone_register);
registerbutton = (Button) findViewById(R.id.button_register);

databasecontroller = new DataBaseController(Realm.getDefaultInstance());
realm = databasecontroller.getRealm();
    }


@Override
protected void onStart() {
super.onStart();


registerbutton.setOnClickListener(new View.OnClickListener() {

@Override
public void onClick(View v) {


/*Busca en la base de datos un usuario y lo coloca en "role"*/
role=databasecontroller.getRealm().where(Role.class).equalTo("username",username.getText().toString()).findFirst();


    /*Si hay algo escrito en el texto auctioneerid*/
    if(!auctioneerid.getText().toString().equals("")) {
        sub1 = databasecontroller.getRealm().where(LiveAuction.class).equalTo("id", Integer.parseInt(auctioneerid.getText().toString())).findFirst();
    }


/*Revisa que se hayan insertado estos datos(datos obligatorios)para procesar el registro de usuario*/
if(!username.getText().toString().equals("") && !password.getText().toString().equals("")
        && !email.getText().toString().equals("") && !phone.getText().toString().equals("") && role==null && sub1==null){
                                                                                                /*Ultima cond añadida*/

intent = new Intent(CreateAccountActivity.this,MainActivity.class);


realm.beginTransaction();
role = realm.createObject(Role.class,username.getText().toString());
role.setPassword(password.getText().toString());
role.setAuctioneerid(auctioneerid.getText().toString());
role.setEmail(email.getText().toString());
role.setPhone(phone.getText().toString());
realm.commitTransaction();



/*Si el id de martillero != nulo entonces inmediatamente creo dos substas con dicho id*/
if( !auctioneerid.getText().toString().equals("") ) {
    realm.beginTransaction();
    sub1 = realm.createObject(LiveAuction.class);
    sub1.setId(Integer.parseInt(auctioneerid.getText().toString()));
    realm.commitTransaction();
}

    realm.beginTransaction();
    sub2 = realm.createObject(LiveAuction.class);
    /*setCountry*/
    realm.commitTransaction();



intent.putExtra("username",username.getText().toString());

startActivity(intent);


}



}
    });

}




}
