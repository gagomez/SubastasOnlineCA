package ids.subastasonlineca.Activities;

import android.app.ListActivity;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;

import java.util.ArrayList;

import ids.subastasonlineca.Controllers.DataBaseController;
import ids.subastasonlineca.Logistic.Auctions.LiveAuction;
import ids.subastasonlineca.Logistic.Role.Role;
import io.realm.Realm;
import io.realm.RealmResults;

public class AuctionMenuActivity extends ListActivity {

private Bundle bundle;
private Intent intent;
private DataBaseController databasecontroller;
private ArrayList<String> ss = new ArrayList();
private Realm realm;
private LiveAuction[] auctionlist;
private LiveAuction auction;
private String user = "";
private Role role;




    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        /*Instancio el controllador de la de datos y le envio la instancia de la base de datos al constructor*/
        databasecontroller = new DataBaseController(Realm.getDefaultInstance());

        /*Instancio la instancia de la base de datos obteniendo la instancia ya guardada en el controlador de la base de datos*/
        realm = databasecontroller.getRealm();



        realm.beginTransaction();
        auction=this.realm.where(LiveAuction.class).equalTo("state","finishied").findFirst();

        if(auction!=null){
            auction.deleteFromRealm();
        }


        realm.commitTransaction();



        /*Crea la lista de la subastas disponibles en la aplicacion (subastas creadas ya en la base de datos)*/
        createauctionlist();


        ArrayAdapter<String> adapter=new ArrayAdapter<String>(
                this, android.R.layout.simple_list_item_1, ss){

            @Override
            public View getView(int position, View convertView, ViewGroup parent) {
                View view =super.getView(position, convertView, parent);

               /*Creo que estas dos lineas de codigo hacen lo mismo*/
               getListView().setBackgroundColor(Color.BLACK);
                parent.setBackgroundColor(Color.BLACK);


                view.setBackgroundColor(Color.BLACK);
                TextView textView=(TextView) view.findViewById(android.R.id.text1);


                textView.setTextColor(Color.WHITE);

                return view;
            }
        };



        /*Pongo las subastas en una lista en la pantalla de menu subastas*/
        setListAdapter(adapter);

        intent = getIntent();
        bundle = intent.getExtras();

        if(bundle!=null){
            /*Obtengo el nombre del usuario que ingreso al sistema*/
            user = (String)bundle.get("username");

            /*Instancio "role" con el objeto de la base de datos que lleva por nombre el usuario que intreso al sistema*/
            role = this.realm.where(Role.class).findFirst();

        }






    }



    @Override
    protected void onListItemClick(ListView l, View v, int position, long id) {
        super.onListItemClick(l, v, position, id);

        String a = l.getItemAtPosition(position).toString();

        realm.beginTransaction();
        auction=this.realm.where(LiveAuction.class).equalTo("id",Integer.parseInt(a)).findFirst();
        auction.setState("finishied");
        realm.commitTransaction();

        if(role.getAuctioneerid().equals(a)){
            intent = new Intent(AuctionMenuActivity.this, AAuctionActivity.class);
            intent.putExtra("username",user);
            startActivity(intent);
        }
        else
        {
            intent = new Intent(AuctionMenuActivity.this, AuctionActivity.class);
            intent.putExtra("username",user);
            startActivity(intent);
        }


    }


/*Crea la lista de subastas ya creadas y puestas en la base de datos*/
public void createauctionlist(){

    RealmResults<LiveAuction> results2 = this.realm.where(LiveAuction.class).findAll();
    auctionlist = results2.toArray(new LiveAuction[results2.size()] );


    for(int i =0 ; i<auctionlist.length ; i++){

        this.realm.beginTransaction();
        ss.add(String.valueOf(auctionlist[i].getId()));
        this.realm.commitTransaction();

    }



}


}/*End de la clase*/


