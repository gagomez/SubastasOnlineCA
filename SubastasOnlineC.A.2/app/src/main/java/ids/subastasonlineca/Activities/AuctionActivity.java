package ids.subastasonlineca.Activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import java.util.ArrayList;

import ids.subastasonlineca.Cronometers.BCronometer;
import ids.subastasonlineca.Logistic.Auctions.LiveAuction;
import ids.subastasonlineca.R;
import io.realm.Realm;

public class AuctionActivity extends AppCompatActivity {


/*Casilla donde se escriben las pujas*/
private EditText bid;

/*Boton de pujar*/
private Button makebid;

/*Boton de actualizar*/
private Button update;

/*Casilla donde se muestra el mejor postor*/
private TextView bestbidder;

/*Casilla donde se muestra la mejor apuesta*/
private TextView bestbid;

/*Array con los postores virtuales*/
public ArrayList<String> virtualbidders ;

/*Array con las cantidades posibles a apostar*/
public ArrayList<Integer> cuantities ;


public String user="";


private Realm realm;
private Intent intent;
private Bundle bundle;
public LiveAuction auction;
public BCronometer cro ;


@Override
protected void onCreate(Bundle savedInstanceState) {
super.onCreate(savedInstanceState);
setContentView(R.layout.activity_auction);

/*Instanciacion de los elementos del Activity*/
bid = (EditText) findViewById(R.id.bid_text);
makebid = (Button) findViewById(R.id.makebid_button);
update = (Button) findViewById(R.id.update_button);
bestbid = (TextView) findViewById(R.id.bestbid_view);
bestbidder = (TextView) findViewById(R.id.bestbidder_view);

realm=Realm.getDefaultInstance();
intent = getIntent();
bundle = intent.getExtras();
auction= new LiveAuction();

virtualbidders = new ArrayList<>();
cuantities = new ArrayList<>();
cro = new BCronometer(this);


setirtualbidders();

    if(bundle!=null){
        user = (String)bundle.get("username");
    }


}

@Override
protected void onStart() {
super.onStart();

cro.start();




        makebid.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if(!bid.getText().toString().equals("")) {
                    int x = Integer.parseInt(bid.getText().toString());


                    auction.setposiblebigerbid(x);
                    auction.setposiblebestbidder(user);
                    act();
                    bid.setText("");

                }



            }
        });


    update.setOnClickListener(new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            act();


            if(cro==null){
                intent = new Intent(AuctionActivity.this, AuctionMenuActivity.class);
                intent.putExtra("username",user);
                startActivity(intent);
            }

        }
    });

    }



/*Actualiza el estado del mejor postor y el de la postura mas alta*/
public void act(){

auction.actbid();

String a = String.valueOf(auction.getbigerbid());
String b = auction.getbestbidder();


bestbid.setText(a);
bestbidder.setText(b);

}


public void setirtualbidders(){

virtualbidders.add("Sergio");
virtualbidders.add("Alexander");
virtualbidders.add("Juanito");

cuantities.add(50);
cuantities.add(100);
cuantities.add(150);
cuantities.add(200);
cuantities.add(250);
cuantities.add(300);

}


public void finishauction(){

    cro.interrupt();
    cro=null;



}




}
