package ids.subastasonlineca.Activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import java.util.ArrayList;

import ids.subastasonlineca.Cronometers.ACronometer;
import ids.subastasonlineca.Logistic.Auctions.LiveAuction;
import ids.subastasonlineca.R;

public class AAuctionActivity extends AppCompatActivity {


/*Boton para adjudicar la subasta*/
private Button award;

/*Boton de actualizar*/
private Button update;

/*Boton para elejir al mejor postor*/
private Button setbestbidder;

/*Casilla donde se muestra el mejor postor*/
private TextView bestbidder;

/*Casilla donde se muestra la mejor apuesta*/
private TextView bestbid;

/*Casilla donde se muestra el nombre de un postor*/
private TextView posiblebestbidder;

/*Casilla donde se muestra una puja*/
private TextView posiblebestbid;

/*Array con los postores virtuales*/
public ArrayList<String> virtualbidders ;

/*Array con las cantidades posibles a apostar*/
public ArrayList<Integer> cuantities ;


private Intent intent;
private Bundle bundle;
public LiveAuction auction;
public ACronometer cro ;



@Override
protected void onCreate(Bundle savedInstanceState) {
super.onCreate(savedInstanceState);
setContentView(R.layout.activity_aauction);


/*Instanciacion de los elementos del Activity*/
award = (Button) findViewById(R.id.award_button);
update = (Button) findViewById(R.id.update_button2);
setbestbidder = (Button) findViewById(R.id.setbestbidder_button);
bestbid = (TextView) findViewById(R.id.bestbid_view2);
bestbidder = (TextView) findViewById(R.id.bestbidder_view2);
posiblebestbid = (TextView) findViewById(R.id.posible_bestbid_view);
posiblebestbidder = (TextView) findViewById(R.id.posible_bestbidder_view);

intent = getIntent();
bundle = intent.getExtras();
auction= new LiveAuction();

virtualbidders = new ArrayList<>();
cuantities = new ArrayList<>();
cro = new ACronometer(this);
posiblebestbid.setText("0");
setirtualbidders();

    }


@Override
protected void onStart() {
super.onStart();

cro.start();




award.setOnClickListener(new View.OnClickListener() {
@Override
public void onClick(View v) {


    String user = "";

    if(bundle!=null){
        user = (String)bundle.get("username");
    }


    intent = new Intent(AAuctionActivity.this, AuctionMenuActivity.class);
    intent.putExtra("username",user);
    startActivity(intent);


}
});


update.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                act(0);
            }
        });

setbestbidder.setOnClickListener(new View.OnClickListener() {
           @Override
            public void onClick(View v) {
            auction.actbid(Integer.parseInt(posiblebestbid.getText().toString())  , posiblebestbidder.getText().toString()  );
            act(1);
        }
    });


}



/*Actualiza el estado del mejor postor y el de la postura mas alta*/
public void act(int x){
String a = "";
String b = "";

if(x==1) {
a = String.valueOf(auction.getbigerbid());
b = auction.getbestbidder();

bestbid.setText(a);
bestbidder.setText(b);

}
else {
    a = String.valueOf(auction.getposiblebigerbid());
    b = auction.getposiblebestbidder();


    if (auction.getposiblebigerbid()>Integer.parseInt(posiblebestbid.getText().toString())) {
    posiblebestbid.setText(a);
    posiblebestbidder.setText(b);
     }


}


}


public void setirtualbidders(){

virtualbidders.add("Sergio");
virtualbidders.add("Alexander");
virtualbidders.add("Juanito");

cuantities.add(50);
cuantities.add(100);
cuantities.add(150);
cuantities.add(200);
cuantities.add(250);
cuantities.add(300);

    }


}
