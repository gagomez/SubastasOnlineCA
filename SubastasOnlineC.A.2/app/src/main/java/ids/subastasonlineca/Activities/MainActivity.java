package ids.subastasonlineca.Activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import ids.subastasonlineca.Controllers.DataBaseController;
import ids.subastasonlineca.Logistic.Role.Role;
import ids.subastasonlineca.R;
import io.realm.Realm;
import io.realm.RealmConfiguration;

public class MainActivity extends AppCompatActivity {


private EditText user;
private EditText password;
private Button joinbutton;
private Button registerbutton;
private Intent intent;
private DataBaseController databasecontroller;
private Role role;



/*Apenas creo esta activity instancio cosas de utilidad*/
@Override
 protected void onCreate(Bundle savedInstanceState) {
 super.onCreate(savedInstanceState);

/*Cuando se instancia esta clase esta linea de codigo pone a la vista del usuario esta pantalla*/
setContentView(R.layout.activity_main);


/*Guardo los componentes del layout en variables*/
user = (EditText) findViewById(R.id.usuario_entrada);
password = (EditText) findViewById(R.id.contraseña_entrada);
joinbutton = (Button) findViewById(R.id.ingresar_boton);
registerbutton = (Button) findViewById(R.id.registrar_boton);

/*Creo la base de datos*/
Realm.init(this);
RealmConfiguration realmConfiguration = new RealmConfiguration.Builder()
            .name("test6")
            .schemaVersion(1)
            .build();

Realm.setDefaultConfiguration(realmConfiguration);


/*Instancio el controlador de la base de datos y le paso la base de datos al constructor*/
databasecontroller = new DataBaseController(Realm.getDefaultInstance());

/*Si quiero borrar la base de datos*/
//databasecontroller.deletedatabase();


}



/*Cuando me muevo a esta activity*/
@Override
protected void onStart() {
super.onStart();



/*Si toco el boton de entrar verifica si mis datos son correctos , si lo son ingreso al sistema y si no lo son me muestra
* un mensaje de error en pantalla*/
joinbutton.setOnClickListener(new View.OnClickListener() {

@Override
public void onClick(View v) {

/*Almacena en un string el texto escrito en los componentes del layout*/
String stringuser = user.getText().toString();
String stringpassword = password.getText().toString();


/*Busca en la base de datos el objeto por el username y lo almacena en "role"*/
role=databasecontroller.getRealm().where(Role.class).equalTo("username",stringuser).findFirst();


    /*Verifica si efectivamente el objeto buscado existe en la base de datos(verifica si ya esta registrado)*/
    if(role!=null) {

             /*Verifica que los datos introducidos son iguales a los datos que contiene el objeto
             * los datos son : el nombre y la contraseña*/
          if (stringuser.equals(role.getUsername()) && stringpassword.equals(role.getPassword())) {


               /*Se usa para cambiar de Activity*/
               /*De donde estoy hacia donde quiero ir*/
              intent = new Intent(MainActivity.this, AuctionMenuActivity.class);


              /*le asigna un dato extra al intent con un nombre y la variable que nos interesa guardar*/
              intent.putExtra("username", stringuser);
              startActivity(intent);
          } else  /*si el nombre del usuario no se encuentra en la base de datos muestro un mensaje de error(usuario no encontrado)*/ {

          }
      }




   }
      });







/*Si toco el boton de registro me muevo hacia la pantalla en la cual se registra un usuario*/
registerbutton.setOnClickListener(new View.OnClickListener() {
@Override
public void onClick(View v) {
intent = new Intent(MainActivity.this,CreateAccountActivity.class);
startActivity(intent);
    }
        });






}/*End del metodo "onStart*/



}/*End de la clase*/
