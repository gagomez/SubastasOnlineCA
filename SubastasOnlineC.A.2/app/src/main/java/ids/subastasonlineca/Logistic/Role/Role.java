package ids.subastasonlineca.Logistic.Role;


import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

public class Role extends RealmObject{

@PrimaryKey
private String username;

private String password;
private String auctioneerid;
private String email;
private String phone;

/*
public Role(String username,String password,String auctioneerid,String email,String phone){

this.username = username;
this.password = password;
this.email = email;
this.phone = phone;


    if(!auctioneerid.equals("")){
        this.auctioneerid = "0";
    }
    else
    {
        this.auctioneerid = auctioneerid;
    }




}*/


    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getAuctioneerid() {
        return auctioneerid;
    }


    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public void setAuctioneerid(String auctioneerid) {
        this.auctioneerid = auctioneerid;
    }
}
