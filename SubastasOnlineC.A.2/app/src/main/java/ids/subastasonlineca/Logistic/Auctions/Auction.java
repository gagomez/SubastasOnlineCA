package ids.subastasonlineca.Logistic.Auctions;

import java.util.ArrayList;
import java.util.Map;

import ids.subastasonlineca.Logistic.Role.Role;

public abstract class Auction {

protected Role Auctioneer;
protected Role Seller;
protected ArrayList<Role> Bidders;
protected Map<String , Integer> items; /*Bienes a subastar*/
protected Map<Integer , Integer> phases;/*El numero de la phase y el tiempo*/
protected int phase;


}
