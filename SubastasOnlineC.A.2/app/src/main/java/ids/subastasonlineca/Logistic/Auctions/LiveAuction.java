package ids.subastasonlineca.Logistic.Auctions;


import io.realm.RealmObject;

public class LiveAuction extends RealmObject {

//private Country country;
private int id = 0;
private int bigerbid=0;
private String bestbidder="";

private int posiblebigerbid=0;
private String posiblebestbidder="";


private String state ="";

/*Actualiza , si es mayor , la puja mas alta y el nombre del postor */
public void actbid(int bb , String bbs) {

        if (bb>bigerbid){
        this.bigerbid = bb;
        this.bestbidder = bbs;
          }
}



public void actbid() {

        if (posiblebigerbid>bigerbid){
            this.bigerbid = posiblebigerbid;
            this.bestbidder = posiblebestbidder;
        }
    }


/*Retorna la puja mas alta*/
public int getbigerbid(){
return (this.bigerbid) ;
}


/*Retorna el nombre del postor con la puja mas alta*/
public String getbestbidder(){
return (this.bestbidder);
}


/*Retorna el id de la subasta*/
public int getId() {
return id;
}


/*Asigna al id de la subasta el id recibido por parametro*/
public void setId(int id) {
this.id = id;
}



public int getposiblebigerbid(){
    return posiblebigerbid;
}



public String getposiblebestbidder(){
return posiblebestbidder;
}



public void setposiblebigerbid(int x){
        this.posiblebigerbid = x;
    }



public void setposiblebestbidder(String a){
        this.posiblebestbidder = a;
    }


    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }
}



